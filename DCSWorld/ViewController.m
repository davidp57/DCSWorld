//
//  ViewController.m
//  DCSWorld
//
//  Created by Julien Rozé on 03/08/13.
//  Copyright (c) 2013 Julien Rozé. All rights reserved.
//

#import "ViewController.h"
#import "Instrument.h"
#import "Datalink.h"
#import "ABRIS.h"

@interface ViewController ()
{
	long tag;
	GCDAsyncUdpSocket *udpSocket;
	
	NSMutableString *log;
}

@end

@implementation ViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithRed:71.0/255.0 green:71.0/255.0 blue:71.0/255.0 alpha:1.0];
    
	// Do any additional setup after loading the view, typically from a nib.
    
    udpSocket = [[GCDAsyncUdpSocket alloc] initWithDelegate:self delegateQueue:dispatch_get_main_queue()];
    
    
    [self bind];
	
    
    // LOAD INSTRUMENTS
    Datalink *datalink = [[Datalink alloc] initWithMainController:self];
    [self.view addSubview:datalink];
    
    ABRIS *abris = [[ABRIS alloc] initWithMainController:self];
    [self.view addSubview:abris];
    
}

-(void)bind {
    NSError *error = nil;
    
    if (![udpSocket bindToPort:23000 error:&error])
	{
		NSLog(@"Error binding: %@", error);
		return;
	}
    if (![udpSocket beginReceiving:&error])
	{
		NSLog(@"Error binding: %@", error);
		return;
	}
}

-(IBAction)send:(id)sender {
    NSData *data = [@"C,9,500,0" dataUsingEncoding:NSUTF8StringEncoding];
    [udpSocket sendData:data toHost:@"10.0.1.22" port:23000 withTimeout:-1 tag:0];
    /*if(!udpSocket.isConnected) {
        NSError *error = nil;
        
        [udpSocket connectToHost:@"10.0.1.17" onPort:23000 error:&error];
    }
    [udpSocket sendData:data withTimeout:-1 tag:0];*/
    
}

-(void)sendCommand:(NSString*)command {
    NSData *data = [command dataUsingEncoding:NSUTF8StringEncoding];
    [udpSocket sendData:data toHost:@"10.0.1.22" port:23000 withTimeout:-1 tag:0];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)udpSocket:(GCDAsyncUdpSocket *)sock didSendDataWithTag:(long)tag
{
	NSLog(@"DATA sent");
}

- (void)udpSocket:(GCDAsyncUdpSocket *)sock didNotSendDataWithTag:(long)tag dueToError:(NSError *)error
{
	// You could add checks here
    NSLog(@"Error sending: %@", error);
}

- (void)udpSocket:(GCDAsyncUdpSocket *)sock didReceiveData:(NSData *)data
      fromAddress:(NSData *)address
withFilterContext:(id)filterContext
{
	NSString *msg = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
	if (msg)
	{
        NSError *error = nil;
        NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        if(error) {
            NSLog(@"BAD JSON: %@ %@", msg, error);
        } else {
            //NSLog(@"RECV: %@", dictionary);
        }
        for(UIView *v in self.view.subviews) {
            if([v isKindOfClass:[Instrument class]]) {
                [((Instrument*)v) updateWithData:dictionary];
            }
        }
	}
	else
	{
		NSString *host = nil;
		uint16_t port = 0;
		[GCDAsyncUdpSocket getHost:&host port:&port fromAddress:address];
		
		NSLog(@"RECV: Unknown message from: %@:%hu", host, port);
	}
}

@end
