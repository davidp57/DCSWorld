//
//  DrawView.m
//  DrawIt
//
//  Created by jroze on 10/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DrawView.h"
#import <QuartzCore/CATiledLayer.h>

@interface DrawView () {
    CGImageRef _lastBackgroundRef;
}

@end

@implementation DrawView

@synthesize delegate = _delegate;
@synthesize paintColor = _paintColor;
@synthesize imagePochoir = _imagePochoir;
@synthesize textureName = _textureName;
@synthesize imageMotif = _imageMotif;
@synthesize buttonBrush = _buttonBrush;
@synthesize buttonBrushSize = _buttonBrushSize;
@synthesize slider = _slider;
@synthesize buttonHome = _buttonHome;
@synthesize buttonPhoto = _buttonPhoto;
@synthesize buttonAlbum = _buttonAlbum;
@synthesize controllerVisible = _controllerVisible;

+ (Class)layerClass {
	return [CATiledLayer class];
}

-(id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
        self.backgroundColor = [UIColor clearColor];
        _trackingPath = NULL;
        self.paintColor = [UIColor redColor];
        _pochoirs = [NSMutableArray array];
        _controllerVisible = NO;
        _pochoirsNames = [NSMutableArray array];
        _motifs = [NSMutableArray array];
        _lastModifs = [NSMutableDictionary dictionary];
        _positionMotifs = [NSMutableArray array];
        
        [self loadCanvas];
        
        lastOffset = CGPointMake(0, 0);
        
        // First
        self.paintColor = [UIColor whiteColor];
        //self.textureName = [UIColor colorWithPatternImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:textureName ofType:@"png"]]];
        
        // Brush Size
        _brushWidth = 40;
        
        CATiledLayer *tiledLayer = (CATiledLayer *)[self layer];
        tiledLayer.levelsOfDetail = 3;
        tiledLayer.tileSize = CGSizeMake(256*[UIScreen mainScreen].scale, 256*[UIScreen mainScreen].scale);
    }
    return self;
}

-(void)loadCanvas {
    
}

-(void)loadAnimation {
    
    
}

-(IBAction)setColor:(UIButton*)sender {
    _slider.hidden = YES;
    
    _paletteView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, self.frame.size.height-_paletteSize.height-1, self.frame.size.width, _paletteSize.height+1)];
    _paletteView.backgroundColor = [UIColor blackColor];
    _paletteView.delegate = self;
    _paletteView.contentSize = _paletteSize;
    _paletteView.contentOffset = lastOffset;
    
    UIImageView *palette = [[UIImageView alloc] initWithFrame:CGRectMake(0, 1, _paletteSize.width, _paletteSize.height)];
    palette.image = [UIImage imageNamed:_paletteName];
    palette.userInteractionEnabled = YES;
    [palette addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectColor:)]];
    [_paletteView addSubview:palette];
    _buttonBrush.hidden = YES;
    _buttonBrushSize.hidden = YES;
    _buttonHome.hidden = YES;
    [self.superview addSubview:_paletteView];
    
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
    lastOffset = scrollView.contentOffset;
}

-(IBAction)setBrushSize:(UIButton*)sender {
    if(_paletteView) {
        [_paletteView removeFromSuperview];
        _paletteView = nil;
    }
    if(_slider.hidden) {
        _slider.hidden = NO;
        _slider.value = _brushWidth;
        _buttonBrush.hidden = YES;
        _buttonHome.hidden = YES;
    } else {
        _slider.hidden = YES;
        _buttonBrush.hidden = NO;
        _buttonHome.hidden = NO;
    }
}

-(IBAction)sliderChanged:(id)sender {
    _brushWidth = round(_slider.value);
}

-(void)selectColor:(UITapGestureRecognizer*)gesture {
    
    if(gesture.state == UIGestureRecognizerStateEnded) {
        
        CGPoint point = [gesture locationInView:gesture.view];
        double step = _paletteSize.width / _paletteCount;
        double i = 0;
        int indexColor = 1;
        while (i <= _paletteSize.width) {
            if(point.x >= i && point.x <= i+step) {
                break;
            }
            indexColor++;
            i += step;
        }
        
        //NSLog(@"color selected is %d", indexColor);
        
        NSString *textureName = [NSString stringWithFormat:@"%@-%d", _paletteName, indexColor];
        self.paintColor = nil;
        self.textureName = [UIColor colorWithPatternImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:textureName ofType:@"png"]]];
        
        [_paletteView removeFromSuperview];
        _paletteView = nil;
        _buttonBrush.hidden = NO;
        _buttonBrushSize.hidden = NO;
        _buttonHome.hidden = NO;
    }
    
}

-(IBAction)setTexture:(UIButton*)sender {
    self.paintColor = nil;
    self.textureName = [UIColor colorWithPatternImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"texture" ofType:@"png"]]];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    CGPoint point = [[touches anyObject] locationInView:self];
    if(_trackingPath == NULL) {
        
        _trackingPath = CGPathCreateMutable();
        CGPathMoveToPoint(_trackingPath, NULL, point.x, point.y);
        //CGPoint prevPoint = CGPathGetCurrentPoint(_trackingPath);
        CGPathAddLineToPoint(_trackingPath, NULL, point.x, point.y);
        
        _lastPoint = point;
        
    }    
}

-(void)drawingStartAtPoint:(CGPoint)point withPochoirName:(NSString*)pochoirName {
    
}

-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    if(_trackingPath != NULL) {
        CGPoint prevPoint = CGPathGetCurrentPoint(_trackingPath);
        CGPoint point = [[touches anyObject] locationInView:self];
        CGPathAddLineToPoint(_trackingPath, NULL, point.x, point.y);
        _lastPoint = point;
        
        int minX = point.x < prevPoint.x ? point.x : prevPoint.x;
        int maxX = point.x > prevPoint.x ? point.x : prevPoint.x;
        int minY = point.y < prevPoint.y ? point.y : prevPoint.y;
        int maxY = point.y > prevPoint.y ? point.y : prevPoint.y;
        
        CGRect dirty = CGRectMake(minX-round((_brushWidth/2)+2), minY-round((_brushWidth/2)+2), (maxX-minX)+_brushWidth+4, (maxY-minY)+_brushWidth+4);
        [self setNeedsDisplayInRect:dirty];
        //[self setNeedsDisplay];
        _trackingDirty = CGRectUnion(dirty, _trackingDirty);
    }
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    
    if(_trackingPath != NULL && _delegate) {
        [_delegate drawView:self finishedTrackingPath:_trackingPath inRect:_trackingDirty];
    }
    if(!arrayOfPaths) {
        arrayOfPaths = [NSMutableArray array];
        
    }
    
    UIBezierPath *path = [UIBezierPath bezierPathWithCGPath:_trackingPath];
    [arrayOfPaths addObject:path];
    
    if(_trackingPath != NULL) {
        CGPathRelease(_trackingPath);
        _trackingPath = NULL;
        _trackingDirty = CGRectZero;
    }
    
}

-(void)erase {
    if(_trackingPath != NULL) {
        CGPathRelease(_trackingPath);
        _trackingPath = NULL;
        _trackingDirty = CGRectZero;
    }
    [self setNeedsDisplay];
}  

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    for(int i=0; i<arrayOfPaths.count; i++) {
        UIBezierPath *path = [arrayOfPaths objectAtIndex:i];
        CGPathRef p = [path CGPath];
        [_paintColor setStroke];
        
        CGContextSetLineCap(ctx, kCGLineCapRound);
        CGContextSetLineWidth(ctx, _brushWidth);
        CGContextSetLineJoin(ctx, kCGLineJoinRound);
        /*CGContextSetAllowsAntialiasing(ctx, YES);
         CGContextSetShouldAntialias(ctx, YES);
         CGContextSetMiterLimit(ctx, 1);*/
        CGContextAddPath(ctx, p);
        CGContextDrawPath(ctx, kCGPathStroke);
    }
    
    if(_trackingPath != NULL) {
        
        
            [_paintColor setStroke];
       
            CGContextSetLineCap(ctx, kCGLineCapRound);
            CGContextSetLineWidth(ctx, _brushWidth);
            CGContextSetLineJoin(ctx, kCGLineJoinRound);
            /*CGContextSetAllowsAntialiasing(ctx, YES);
            CGContextSetShouldAntialias(ctx, YES);
            CGContextSetMiterLimit(ctx, 1);*/
            CGContextAddPath(ctx, _trackingPath);
            CGContextDrawPath(ctx, kCGPathStroke);
        //}
        //NSLog(@"drawing done.");
    }
}

@end
