//
//  main.m
//  DCSWorld
//
//  Created by Julien Rozé on 03/08/13.
//  Copyright (c) 2013 Julien Rozé. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
